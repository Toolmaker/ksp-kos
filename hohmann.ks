runoncepath("0:/math").

function calculateDeltaVToAltitude {
    parameter newAlt, at.

    local o is OrbitAt(ship, at).
    local a is o:body:AltitudeOf(o:position).
    local r1 is orbit:body:radius + a.
    local r2 is orbit:body:radius + newAlt.
    local mu is orbit:body:mu.

    local v1 is Sqrt(mu / r1) * ((Sqrt(2 * r2 / (r1 + r2)) - 1)).

    return v1.
}

function calculateDeltaVToCircularize {
  parameter orbit.
 
  local v is VelocityAt(ship, time:seconds + eta:apoapsis):orbit:mag.
  local r1 is orbit:body:radius.
  local r2 is r1 + apoapsis.
  local mu is orbit:body:mu.

  local v1 is v.
  local v2 is Sqrt(mu / r2) * (1 - Sqrt(2 * r1) / (r1 + r2)).
 
  print "Estimated DeltaV: " + (v2 - v1).
  return (v2 - v1).  
}

function getWeightedIsp {
  local isp is 0.
  list engines in myEngines.
  for en in myEngines {
    if (en:ignition and not en:flameout) {
      set isp to isp + (en:isp * (en:availablethrust / ship:availablethrust)).
    }
  }

  return isp.
}

function getEstimatedBurnTime {
    parameter deltaV.

    local g is 9.80665.
    local isp is getWeightedIsp().
    local mf is ship:mass / constant:e^(deltaV / (isp * g)).
    local fuelFlow is ship:availablethrust / (isp * g).
    return (ship:mass - mf) / fuelFlow.
}