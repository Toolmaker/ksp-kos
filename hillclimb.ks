
// function improveConverge {
//   parameter mnv, scoreFn.

//   local score is scoreFn(mnv).
//   for stepSize in list(100, 10, 1) {
//     until false {
//       local prevScore is score.
//       set result to improveManeuver(mnv, score, stepSize, scoreFn).
//       set mnv to result[0].
//       set score to result[1].

//       if (score >= prevScore) {
//         break.
//       }
//     }
//   }

//   return mnv.
// }

// function improveManeuver {
//   parameter current, scoreToBeat, stepSize, scoreFunction.

//   local bestCandidate is current.
//   local bestScore is scoreToBeat.
//   local candidates is list().
//   local index is 0.

//   until index >= current:length {
//     local inc is current:copy().
//     local dec is current:copy().

//     set inc[index] to current[index] + stepSize.
//     set dec[index] to current[index] - stepSize.

//     candidates:add(inc).
//     candidates:add(dec).

//     set index to index + 1.
//   }

//   for candidate in candidates {
//     local score is scoreFunction(candidate).
//     if (score < bestScore) {
//       set bestScore to score.
//       set bestCandidate to candidate.
//     }
//   }

//   return list(bestCandidate, bestScore).
// }

// function guardAgainstPastManeuvers {
//   parameter scoreFunction.

//   local fn is {
//     parameter data.
//     if data[0] < time:seconds + 15 {
//       return 2^64.
//     }

//     return scoreFunction(data).
//   }.

//   return fn@.
// }

// function getManeuverScore {
//   parameter mnvList.

//   local mnv is node(mnvList[0], mnvList[1], mnvList[2], mnvList[3]).
//   addManeuverToFlightPlan(mnv).
//   local score is mnv:orbit:eccentricity.
//   removeManeuverFromFlightPlan(mnv).

//   return score.
// }