runoncepath("0:/hohmann.ks").

// TODO: Slowly reduce throttle when approaching the end of a maneuver burn.
// TODO: Replace the use of maneuver nodes with custom routines.

function doStage {
  wait until stage:ready.
  stage.
}

function executeManeuver {
  print "Executing maneuver burn".
  parameter mnvData.
  local mnv is node(mnvData[0], mnvData[1], mnvData[2], mnvData[3]).
  addManeuver(mnv).

  lockSteeringAtManeuverTarget(mnv).
  print "Waiting until 10s before start".
  local startTime is calculateStartTime(mnv).
  wait until time:seconds > startTime - 10.
  
  lockSteeringAtManeuverTarget(mnv).

  wait until time:seconds > startTime.

  print "Beginning burn".
  lock throttle to 1.
  until isManeuverComplete(mnv) {
    local acceleration is getAcceleration().
    local maxAcceleration is getMaxAvailableAcceleration().

    // if (mnv:deltaV:mag > (maxAcceleration * 2)) {
    //   clearscreen.
    //   print "Thrust is        : " + ship:availablethrust.
    //   print "Throttle         : " + throttle.
    //   print "Acceleration     : " + acceleration.
    //   print "Max acceleration : " + maxAcceleration.
    //   print "Remaining deltaV : " + mnv:deltaV:mag.
    // }

    if (mnv:deltaV:mag < (maxAcceleration * 2)) {
      local t is MAX(mnv:deltaV:mag / (maxAcceleration * 2), 0.05).
      // print "Acceleration     : " + acceleration.
      // print "Max acceleration : " + maxAcceleration.      
      // print "New throttle     : " + t.
      set throttle to t.
    }

    autoStage().
  }

  print "Burn completed".
  lock throttle to 0.

  unlock steering.
  removeManeuver(mnv).
}

function addManeuver {
  parameter mnv.
  
  add mnv.
}

function calculateStartTime {
  parameter mnv.

  return time:seconds + mnv:eta - getEstimatedBurnTime(mnv:burnvector:mag) / 2.
}

function lockSteeringAtManeuverTarget {
  parameter mnv.
  
  lock steering to mnv:burnvector.
}

function isManeuverComplete {
  parameter mnv.
  
  if not (defined originalVector) or originalVector = -999 {
    declare global originalVector to mnv:burnvector.
  }

  if vang (originalVector, mnv:burnvector) > 90 {
    set originalVector to -999.
    return true.
  }

  return false.
}

function removeManeuver {
  parameter mnv.
  remove mnv.
}

function launch {
  print "Launching".
  lock throttle to 1.
  doStage().
  doStage().
}

function doAscent {
  lock targetPitch to 88.963 - 1.03287 * alt:radar^0.409511.
  set targetDirection to 90.
  lock steering to heading(targetDirection, targetPitch).
}

function doShutdown {
  print "Shutting down program.".
  lock throttle to 0.
  lock steering to prograde.
  wait until false.
}

function getAcceleration {
  return (ship:availablethrust / (ship:mass)) * throttle.
}

function getMaxAvailableAcceleration {
  return ship:availablethrust / (ship:mass).
}

function autoStage {
  if not(defined oldThrust) {
    declare global oldThrust to ship:availablethrust.
  }

  if ship:availablethrust < (oldThrust - 10) {
    print "Auto staging".
    doStage().
    wait 1.
    if (ship:availablethrust = 0) {
      doStage().
      wait 1.
    }

    set oldThrust to ship:availablethrust.
  }
}

function getCircularizationManeuver {
  return list(time:seconds + eta:apoapsis, 0, 0, calculateDeltaVToCircularize(ship:orbit)).
}

function executeTransLunarInjection {
  print "Obtaining translunar injection manuever".
  local mnvData is getTranslunarInjectionManeuver().

  executeManeuver(mnvData).
}

function getTranslunarInjectionManeuver {
  local mnvData is getInitialTranslunarInjectionManeuver().

  local invalid is 2^32.
  local stepSize is 0.1.
  local requiredPeriapsis is 15000.
  local prevPeriapsis is invalid.
  
  until false {
    local node is node(mnvData[0], mnvData[1], mnvData[2], mnvData[3]).
    add node.
    local nextPatch is node:orbit:HasNextPatch.
    local nextPeriapsis is 0.
    if (nextPatch) {
      set nextPeriapsis to node:orbit:nextPatch:periapsis.
    }

    remove node.

    if (nextPatch) {
      print "Next patch found. Next periapsis: " + nextPeriapsis.
      if (nextPeriapsis > requiredPeriapsis) {
        print "Subtracting " + stepSize.
        set mnvData[3] to mnvData[3] - stepSize.
      } else if (nextPeriapsis < requiredPeriapsis) {
        print "Adding " + stepSize.
        set mnvData[3] to mnvData[3] + stepSize.
      }

      if (prevPeriapsis <> invalid) {
        if (nextPeriapsis < requiredPeriapsis and prevPeriapsis > requiredPeriapsis) {
          if (stepSize <> 0.1) {
            print "Changing stepsize to 0.1".
            set stepSize to 0.1.
          } else {
            if (nextPeriapsis > 0) { 
              return mnvData. 
            } else {
              set mnvData[3] to mnvData[3] + stepSize.
              return mnvData.
            }
          }
        } else if (nextPeriapsis > requiredPeriapsis and prevPeriapsis < requiredPeriapsis) {
          if (stepSize <> 0.1) {
            print "Changing stepsize to 0.1".
            set stepSize to 0.1.
          } else {
            if (nextPeriapsis > 0) { 
              return mnvData. 
            } else {
              set mnvData[3] to mnvData[3] - stepSize.
              return mnvData.
            }
          }        
        }
      }

      set prevPeriapsis to nextPeriapsis.
    } else {
      print "No next patch found!".
    }
  }
}

function getInitialTranslunarInjectionManeuver {
    local transferPerihelion is 11400000 + (Kerbin:Radius).// + (2*Mun:Radius) + 50000.
    local transferAphelion is periapsis.
    local transferSma is getSemiMajorAxis(Kerbin, transferPerihelion, transferAphelion).
    local transferPeriod is getOrbitalPeriod(Kerbin:mu, transferSma) / 2.
    
    local mnvData is list().
    local deltaV is calculateDeltaVToAltitude(transferPerihelion, periapsis).
      
    print "Waiting until ideal transfer window has come up".
    until false {
      local orbitToMun is getOrbitAngleBetween(mun, ship, kerbin).  
      local predictedBurnTime is getEstimatedBurnTime(deltaV).
      local preferredTime is time:seconds + 30 + (predictedBurnTime / 2).
      local predictedAngle is getOrbitAngleBetweenAt(mun, ship, kerbin, preferredTime).
      local angularDiameterOfBody is getAngularDiameterOfBody(kerbin, mun).
      local ejectionAngle is getEjectionAngleToMun(transferPeriod) - angularDiameterOfBody.      
      
      if (predictedAngle >= ejectionAngle - 2.5 and predictedAngle <= ejectionAngle + 1) {
        set warp to 0.
      }

      if (predictedAngle >= ejectionAngle - 0.05) and (predictedAngle <= ejectionAngle + 0.05) {
        return list(time:seconds + 30 + (predictedBurnTime / 2), 0, 0, deltaV).
      }
    }  
}

function warpToMunarTransition {
  local nextEta is ship:orbit:nextPatchEta.
  print "TLI completed. Awaiting transition to next orbit patch in " + nextEta + " seconds.".
  wait 1.
  KUniverse:TimeWarp:WarpTo(time:seconds + nextEta - 20).
  wait nextEta.

  print "Transition in " + ship:orbit:nextPatchEta + " seconds.".
  local timeToWait is ship:orbit:nextPatchEta + 5.
  wait timeToWait.
}

function circularizeAroundMun {
  local timeToPeriapsis is eta:periapsis.
  local vAtPeriapsis is VelocityAt(ship, time:seconds + timeToPeriapsis):orbit:mag.
  local orbitVelocity is getOrbitalSpeedAtAltitude(ship:orbit:body, periapsis).
  local speedChange is (orbitVelocity - vAtPeriapsis).

  print "Speed at periapsis: " + vAtPeriapsis.
  print "Speed for circular orbit: " + orbitVelocity.
  print "Need " + speedChange + " m/s".
  print "Periapsis in " + eta:periapsis + " seconds.".

  local mnvData is list(time:seconds + eta:periapsis, 0, 0, speedChange).
  local burnTime is getEstimatedBurnTime(Abs(speedChange)).
  local timeToBurn is (eta:periapsis - (burnTime / 2) - 40).
  KUniverse:TimeWarp:WarpTo(time:seconds + timeToBurn).
  wait timeToBurn.

  executeManeuver(mnvData).
}

function main {
  clearscreen.
  // launch().
  // doAscent().

  // until apoapsis > 100000 {
  //   autoStage().
  // }

  // print "Main engine cut off.".
  // lock throttle to 0.
  // lock steering to prograde.

  // wait until alt:radar > 70000.
  // panels on.
  // local mnv is getCircularizationManeuver().
  // executeManeuver(mnv).

  executeTransLunarInjection().
  warpToMunarTransition().
  circularizeAroundMun().

  doShutdown().
}

main().