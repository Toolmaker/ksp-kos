function getSemiMajorAxis {
    parameter selectedBody, perihelion, aphelion.

    local radius is selectedBody:radius.

    return ((2 * radius) + perihelion + aphelion) / 2.
}

function getOrbitalPeriod {
    parameter mu, semiMajorAxis.

    return (2 * constant:pi) * Sqrt((semiMajorAxis^3) / mu).
}

function getEjectionAngleToMun {
    parameter transferTime.

    return 180 - (360 / Mun:RotationPeriod * transferTime).
}

function getOrbitAngleBetween {
    parameter origin, targ, parentBody.

    return getOrbitAngleBetweenAt(origin, targ, body, time:seconds).
}

function getOrbitAngleBetweenAt {
    parameter origin, targ, parentBody, atTime.

    local originPosition is PositionAt(origin, atTime) - parentBody:position.
    local targetPosition is PositionAt(targ, atTime) - parentBody:position.
    local currentAngle to vang(originPosition, targetPosition).
    local originVelocity is origin:velocity:orbit.
    local originNormal is vcrs(originVelocity, originPosition).
    local originTargetCross is vcrs(originPosition, targetPosition).

    if vdot(originNormal, originTargetCross) < 0 { 
        return 0 - currentAngle. 
    }

    return currentAngle.    
}

function getAngularDiameterOfBody {
    parameter origin, targetBody.

    local diameter is targetBody:radius.
    local distance is getDistanceToPosition(origin:position, targetBody:position).

    return 2 * arctan2(diameter, 2 * distance). 
}

function getAngularDiameterOfSoi {
    parameter origin, targetBody.

    local diameter is targetBody:soiradius.
    local distance is getDistanceToPosition(origin:position, targetBody:position).

    return 2 * arctan2(diameter, 2 * distance). 
}

function getDistanceToPosition {
    parameter origin, targ.

    local x is targ:x - origin:x.
    local y is targ:y - origin:y.
    local z is targ:z - origin:z.

    return Sqrt(x*x + y*y + z*z).
}

function getOrbitalSpeedAtAltitude {
    parameter selectedBody, requiredAltitude.

    local mu is selectedBody:mu.
    local radius is selectedBody:radius.
    local a is getSemiMajorAxis(selectedBody, requiredAltitude, requiredAltitude).
    return Sqrt(mu * ((2 / (radius + requiredAltitude)) - (1 / a))).
}